<?php

namespace App\NotificationChannels\Payamak;

use Pamenary\LaravelSms\Sms;
use Pamenary\LaravelSms\Gateways\GatewayInterface;

class PayamakMessage
{
    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $recipient;

    /**
     * @var boolean
     */
    public $isFlash;

    /**
     * constructor.
     * @var string content
     * @var string recipient
     * @var bool isFlash
     */
    public function __construct(string $content, string $recipient, bool $isFlash = False) {
        $this->content = $content;
        $this->recipient = $recipient;
        $this->isFlash = $isFlash;
    }

    /**
     * to function
     * @var string recipient
     */
    public function to(string $recipient) {
        $this->recipient = $recipient;
    }
}