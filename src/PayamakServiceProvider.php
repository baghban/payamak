<?php
namespace App\NotificationChannels\Payamak;

use Illuminate\Support\ServiceProvider;
use Pamenary\LaravelSms\Sms;

class PayamakServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->app->when(PayamakChannel::class)
            ->needs(Sms::class)
            ->give(function () {
                return new Sms();
            });
    }
    /**
     * Register any package services.
     */
    public function register()
    {
    }
}