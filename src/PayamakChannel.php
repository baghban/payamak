<?php

namespace App\NotificationChannels\Payamak;

use Illuminate\Notifications\Notification;
use Pamenary\LaravelSms\Sms;
use App\NotificationChannels\Payamak\PayamakMessage;

class PayamakChannel
{
    /**
     * @var Sms
     */
    protected $sms;

    /**
     * Channel constructor.
     * @var Sms sms
     */
    public function __construct(Sms $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toPayamak($notifiable);
        $this->sms->sendSMS((array) $message->recipient, $message->content);
    }
}