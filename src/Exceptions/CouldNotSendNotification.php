<?php
namespace App\NotificationChannels\Payamak\Exceptions;
class CouldNotSendNotification extends \Exception
{
    public static function serviceRespondedWithAnError($response)
    {
        return new static("Descriptive error message.");
    }
}