# Payamak Notifications Channel for Laravel 5.3 [WIP]


This package makes it easy to send SMS notification using [laravel-sms](https://packagist.org/packages/baghban/laravel-sms) with Laravel 5.3.

## Credits

- [Mojtaba Baghban](https://framagit.org/baghban/)
## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
